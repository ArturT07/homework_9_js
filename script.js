function createListFromArray(arr, parent = document.body) {
    const list = document.createElement("ul");

    arr.forEach((item) => {
        const listItem = document.createElement("li");
        if (Array.isArray(item)) {
            listItem.appendChild(createListFromArray(item));
        } else {
            listItem.textContent = item;
        }
        list.appendChild(listItem);
    });

    return list;
}

const array1 = ["hello", "world", ["Kiev", "Kharkiv", "Odessa", "Lviv", "Dnieper"]];
const list1 = createListFromArray(array1);
document.body.appendChild(list1);

const array2 = ["1", "2", "3", "sea", ["user", 23], "city"];
const customParentElement = document.getElementById("customParent");
const list2 = createListFromArray(array2);
customParentElement.appendChild(list2);

let timer;
let remainingTime = 3;

function hideListAfterThreeSeconds() {
    const timerElement = document.getElementById("timer");
    timerElement.style.display = "block";
    timerElement.innerText = `This list will disappear in ${remainingTime} seconds.`;

    timer = setInterval(() => {
        remainingTime--;
        timerElement.innerText = `This list will disappear in ${remainingTime} seconds.`;
        if (remainingTime <= 0) {
            list1.style.display = "none";
            list2.style.display = "none";
            document.getElementById("showButton").style.display = "block";
            clearInterval(timer);
        }
    }, 1000);
}

function showListAgain() {
    list1.style.display = "block";
    list2.style.display = "block";
    document.getElementById("showButton").style.display = "none";
    document.getElementById("timer").style.display = "block";
    remainingTime = 3;
    hideListAfterThreeSeconds();
}

document.getElementById("showButton").addEventListener("click", () => {
    showListAgain();
});

hideListAfterThreeSeconds();